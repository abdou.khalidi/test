FROM odoo:latest

USER root
RUN apt update && apt install -y --no-install-recommends git

WORKDIR /tmp/
COPY ./ /tmp/
RUN python setup.py install

USER odoo


